# Slidev bibliography addons

This addons provides a `Sources` and `Source` component to show sources.

## Setup

1. Install the addon with your favorite package manager
2. Install `pinia` as a dependency with your favorite package manager
3. Add it in your frontmatter:
```
addons:
  - slidev-addon-bibliography
```
3. Create a `bibliography.json` file in the root of your slidev presentation with an empty object (`{}`).
4. Create a `setup/main.ts` file with the following contents:
```ts
import { defineAppSetup } from '@slidev/types'
import Bibliography from '../bibliography.json'
import { createPinia } from 'pinia'

const pinia = createPinia()

export default defineAppSetup(({ app }) => {
    app.use(pinia)
    app.provide('bibliography', Bibliography)
})
```

## Usage

In the `bibliography.json` file, add sources like this:

```json
{
  "ot": {
    "url": "https://en.wikipedia.org/wiki/Operational_transformation"
  },
  "crdtPaper": {
    "title": "Nuno Preguiça: Conflict-free Replicated Data Types: An Overview. arXiv:1806.10254, June 2018."
  }
}
```

Each source can have the following attributes:

* `title` (mandatory)
* `url`
* `author`
* `accessed` (the date you've accessed the source)
* `date` (the date the source was published)

In your slides, reference a source like this:

```md
* Something <Source item="ot"/>
```

It will then show up in the slide with a reference like `[0]`.

On the slide where you want to include all used sources, simply add the `<Sources/>` component.
It will show all sources you've used in order of appearance.

## License

MIT
