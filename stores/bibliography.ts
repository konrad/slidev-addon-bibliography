import { defineStore } from 'pinia'

export const useBibliographyStore = defineStore('bibliography', {
    state: () => {
        return {
            sources: new Set(),
        }
    },

    actions: {
        addSource(source: string) {
            this.sources.add(source)
        },
    },
})